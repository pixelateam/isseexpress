(function() {
  'use strict';

  angular
    .module('isseExpress')
    .controller('MainController', ['$scope', '$window', function($scope, $window){
      $scope.showMap = false;

      $scope.places = {
        'Xela' : {
          coordinates: [14.844820, -91.527540]
        },
        'Antigua' : {
          coordinates: [14.555288, -90.735630]
        },

        'Guatemala' : {
          coordinates: [14.601844, -90.524751]
        },
        'Mazatenango': {
          coordinates: [14.534064, -91.503626]
        }
      };

      $scope.mapStyles = [{
        stylers: [
          { hue: '#3785CF'},
          { visibility: 'simplified'}
        ]
      },
      {
         featureType: 'transit.station.bus',
         stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'road',
        elementType: 'labels',
        stylers: [{ visibility: 'off'}]
      }];


      $scope.currentPlace = $scope.places['Xela'];

      $scope.swipeUp = function(){
        $scope.$broadcast('transition', 1);
      };

      $scope.swipeDown = function(){
        $scope.$broadcast('transition', -1);
      };

      $scope.toggle = function(){
        $scope.showMap = ! $scope.showMap;
      };

      $scope.goTo = function(index){
        index = index || 0;
        $scope.$broadcast('transition-to', index)
      };

      $scope.openWindow = function(provider, code){
        if(angular.isDefined(provider)){
          var source = provider.url.replace(/%num/gi, code);

          $window.open(source, '_blank');
        }
      };

      $scope.updateMap = function(place){
        $scope.currentPlace = $scope.places[place.title];
      };

    }]);
})();
