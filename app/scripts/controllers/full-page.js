(function(){
  'use strict';

  var app = angular.module('isseExpress');

  app.controller('FullPageSectionsController', ['$scope', '$document','$timeout',function($scope, $document, $timeout){
    var sections = [];
    $scope.totalSectons = 0;
    $scope.currentSectionIndex = 0;
    $scope.inTransition = false;
    $scope.quietPeriod = false;

    $scope.viewFinderElement = null;

    $scope.beforeTransitionCallback = function(object){
      sections[object.fromSection].scope.leave();
      sections[object.toSection].scope.enter();
    }

    function normalizeIndex(index) {
      if (index <= 0) {
           return 0;
       } else if(index >= $scope.totalSectons){
         return $scope.totalSectons;
       }
       else{
           index = index % $scope.totalSectons;
       }

       return index;
    }

    this.moveTo = function(index) {
      var fromSection = normalizeIndex($scope.currentSectionIndex);
      var toSection = normalizeIndex(index);

      if(toSection >= $scope.totalSectons || fromSection === toSection || toSection < 0){
        return;
      }

      if($scope.beforeTransition !== null
          && angular.isFunction($scope.beforeTransition)){
        $scope.beforeTransition({toSection: toSection, fromSection:  fromSection});
      }
      $scope.beforeTransitionCallback({toSection: toSection, fromSection:  fromSection});

      $scope.inTransition = true;
      $scope.quietPeriod = true;

      $document.on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e){
        var elClass = e.target.getAttribute('class') || 'not-a-class';
        if(angular.isDefined(elClass) && elClass.includes('pill')){
          return;
        }

        $scope.$apply(function(){
          $scope.inTransition = false;
          if ($scope.afterTransition !== null
                      && angular.isFunction($scope.afterTransition)) {

            $scope.afterTransitionCallback({fromSection: fromSection, toSection: toSection});
          }

          $timeout(function(){
            $scope.$apply(function(){
              $scope.quietPeriod = false;
              if ($scope.afterQuietPeriod !== null
                                && angular.isFunction($scope.afterQuietPeriod)) {

                $scope.afterQuietPeriodCallback({fromSection: fromSection, toSection: toSection});
              }
            });
          }, 500);

        });
      });

      $scope.currentSectionIndex = index;

      var offset = toSection * 100;

      $scope.viewFinderElement.css("transform", "translate3d(0, -" + offset + "%, 0)");
      $scope.viewFinderElement.css("-webkit-transform", "translate3d(0, -" + offset + "%, 0)");
      $scope.viewFinderElement.css("-moz-transform", "translate3d(0, -" + offset + "%, 0)");
      $scope.$emit('current-section', sections[$scope.currentSectionIndex]);
    };

    this.addSection = function(section, element){
      if($scope.totalSectons === 0){
        section.state = 'entering';
      }

      section.indexOf = $scope.totalSectons++;
      element.addClass('full-page-item');
      element.css("top", (section.indexOf * 100) + '%');
      var id = element[0].attributes['id'];
      if(angular.isDefined(id)) id = id.value;
      sections.push({scope: section, id: id});
    }

    this.removeSlide = function(index) {
     $scope.slides.splice(index, 1);
     $scope.numSlides--;
    };
  }]);
})();
