(function() {
  'use strict';

  angular
    .module('isseExpress')
    .controller('ScrollSpyController', ['$scope', function($scope){
      $scope.spies = [];
      $scope.viewElement = null;

      this.addSpy = function (spyObj) {
        return $scope.spies.push(spyObj);
      };

      this.in = function(klass, id){
        $scope.viewElement.addClass(klass);
        $scope.$emit('class-changed', {klass: klass, id: id});
      };

      this.out = function(klass){
        $scope.viewElement.removeClass(klass);
      };
    }]);
})();
