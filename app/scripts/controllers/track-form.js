(function() {
  'use strict';

  angular
    .module('isseExpress')
    .controller('TrackingCodeFormController', ['$scope', function($scope){
      $scope.data = {};

      var patterns = [{
        patterns: [
          /\b(1Z ?[0-9A-Z]{3} ?[0-9A-Z]{3} ?[0-9A-Z]{2} ?[0-9A-Z]{4} ?[0-9A-Z]{3} ?[0-9A-Z]|[\dT]\d\d\d ?\d\d\d\d ?\d\d\d)\b/i,
          /\b\d{9}\b/i
        ],
        provider: 'UPS',
        url: 'http://wwwapps.ups.com/WebTracking/processInputRequest?InquiryNumber1=%num&sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&track.x=0&track.y=0'
      },
      {
        patterns: [
          /\b96[0-9]{20}\b/i,
          /\b[0-9]{15}\b/i,
          /\b[0-9]{12}\b/i,
          /\b((98\d\d\d\d\d?\d\d\d\d|98\d\d) ?\d\d\d\d ?\d\d\d\d( ?\d\d\d)?)\b/i,
          /^[0-9]{12}$/i
        ],
        provider: 'FedEx',
        url: 'http://www.fedex.com/Tracking?tracknumbers=%num&action=track&language=spanish'
      },
      {
        pattern: /^[0-9]{10}$/i,
        provider: 'DHL',
        url: 'http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB=%num'
      },
      {
        pattern: /\b(91\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d|91\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d)\b/i,
        provider: 'USPS',
        url: 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=%num'
      },
      {
        patterns: [/copt/i, /awesome/i],
        provider: 'Pixela',
        url: '//pixela.com.gt/?ref=lol-%num'
      }];

      var matchCode = function(code){
        var provider, shouldContinue=true;
        angular.forEach(patterns, function(pattern, key){
          if(shouldContinue){
            if(angular.isDefined(pattern.pattern)){
              if(code.match(pattern.pattern)){
                provider = pattern;
                shouldContinue = false;
              }
            }
            else if(angular.isDefined(pattern.patterns)){
              pattern.patterns.forEach(function(value, index, array){
                if(shouldContinue){
                  if(code.match(value)){
                    provider = pattern;
                    shouldContinue = false;
                  }
                }
              });
            }
          }
        });

        return provider;
      };

      $scope.createLink = function(){
        var provider = matchCode($scope.data.code);

        if(angular.isUndefined(provider) || provider === null){
          $scope.message = "No pudimos rastrear tu paquete";
        }
        else if($scope.afterSubmit !== null && angular.isFunction($scope.afterSubmit)){
          $scope.afterSubmit(provider, $scope.data.code);
        }

        if(provider.provider === 'Pixela'){
          $scope.message = ":)";
        }
      };
    }]);
})();
