(function(){
  'use strict';

  var app = angular.module('isseExpress');

  app.controller('ShowCaseController', function($scope){
    var items = $scope.items = [];

    $scope.select = function(item) {

       angular.forEach(items, function(item) {
         item.selected = false;
       });
       item.selected = true;
       $scope.selectedItem = item;
       if($scope.selectedItemChanged !== null &&
            angular.isDefined($scope.selectedItemChanged)){
              $scope.selectedItemChanged(item);
            }
     };

    this.addItem = function(item){
      if(items.length === 0){
        $scope.select(item);
      }
      items.push(item);
    }
  });
})();
