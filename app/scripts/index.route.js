(function() {
  'use strict';

  angular.module('isseExpress')
    .config(function($routeProvider, hammerDefaultOptsProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/templates/main.html',
        controller: 'MainController'
      })
      .otherwise({
        redirectTo: '/'
      });

      hammerDefaultOptsProvider.set({
        recognizers: [[Hammer.Swipe, { time: 100}]]
    });
  })

})();
