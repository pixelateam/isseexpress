(function(){
   'use strict';
   angular.module('isseExpress')
   .directive('spy', function(){
       return {
         restrict: 'A',
         require: '^scrollSpy',
         link: function(scope, element, attrs, scrollSpy){
           var sibling = null;
           attrs.spyClass = attrs.spyClass || 'current';
           attrs.siblingsClass = attrs.siblingsClass || 'current';


           scrollSpy.addSpy({
             id: attrs.spy,
             in: function(){
               scrollSpy.in(attrs.siblingsClass);
               element.addClass(attrs.spyClass);
             },
             out: function(){
               scrollSpy.out(attrs.siblingsClass);
               element.removeClass(attrs.spyClass);
             }
           });
         }
       };
     });
})();
