(function(){
  'use strict';

  angular.module('isseExpress')
    .directive('scrollSpy', [function(){
      return {
        restrict: 'A',
        controller: 'ScrollSpyController',
        link: function(scope, element, attrs){
          var spyElems = [];

          scope.viewElement = element;

          scope.$watch('spies', function(value){
            angular.forEach(scope.spies, function(spy, key){
              if(! spyElems[spy.id]){
                spyElems[spy.id] = angular.element('#'+spy.id);
              }
            });
          });

          scope.$on('current-section', function(event, section){
            angular.forEach(scope.spies, function(spy, key){
              spy.out();
              
              var id = section.id;

              if(id !== null && angular.isDefined(id)){
                if(spyElems[id] && spy.id === id) {
                  spy.in();
                }
              }
            });
          });
        }
      };
    }]);
})();
