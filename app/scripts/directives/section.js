(function(){
  'use strict';

  angular.module('isseExpress')
    .directive('pageSection', function(){
      return {
        require: '^^fullPageSections',
        restrict: 'E',
        transclude: true,
        scope:{
          enterClass: '=?',
          leaveClass: '=?'
        },
        templateUrl: '/templates/section.html',
        controllerAs: 'sectionCtrl',
        link: function(scope, element, attrs, sectsCtrl){
          sectsCtrl.addSection(scope, element);

          scope.enter = function(){
            var els = element[0].getElementsByClassName('animated');

            angular.forEach(els, function(value, key){
              var el = angular.element(value);
              el.addClass(scope.enterClass || 'bounceIn');
              el.removeClass(scope.leaveClass || 'bounceOut');
            });
          };

          scope.leave = function(){
            var els = element[0].getElementsByClassName('animated');

            angular.forEach(els, function(value, key){
              var el = angular.element(value);
              el.addClass(scope.leaveClass || 'bounceOut');
              el.removeClass(scope.enterClass || 'bounceIn');
            });
          };

          element.on('$destroy', function(){
            slidesController.removeSlide(scope.indexOf);
          });
        }
      }
  });
})();
