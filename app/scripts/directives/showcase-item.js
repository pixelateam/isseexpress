(function(){
  'use strict';

  angular.module('isseExpress')
    .directive('showCaseItem', function(){
      return {
        require: '^^showCase',
        restrict: 'E',
        transclude: {
          'content': 'itemInfo',
          'picture': '?itemPicture'
        },
        scope: {
          title: '@'
        },
        templateUrl: '/templates/showcase-item.html',
        link: function(scope, element, attrs, showCaseCtrl){
          showCaseCtrl.addItem(scope)
        }
      }
  });
})();
