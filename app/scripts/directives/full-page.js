(function(){
  'use strict';

  angular.module('isseExpress')
  .directive('fullPageSections',['$document', '$timeout', function($document,  $timeout){
    return {
      restrict: 'E',
      transclude: true,
      controller: 'FullPageSectionsController',
      templateUrl: '/templates/full-page.html',
      scope: {
        beforeTransition: '=ssbefore',
        afterTransition:  '=ssAfter',
        afterQuietPeriod: '=ssAfterQuiet'
      },
      link: function(scope, element, attrs, controller){
        scope.viewFinderElement = element;

        element.addClass('full-page-root');

        var inTransition = false;

        scope.$on('transition', function(event, offset){
          offset = parseInt(offset);

          if(! angular.isNumber(offset)){
            return;
          }

          controller.moveTo(scope.currentSectionIndex + offset);
        })

        scope.$on('transition-to', function(event, index){
          index = parseInt(index);

          if(! angular.isNumber(index)){
            return;
          }

          controller.moveTo(index);
        })

        $document.on('keydown', function(event){
          var tag = event.target.tagName.toLowerCase();

          if(tag == 'input' || tag == 'textarea' || tag == 'select'){
            return true;
          }

          var moveForward = true;
          var offset = 0;

          switch(event.which){
            case 33: // Page Up
            case 38: // Up Key
              offset = -1;
              break;

            case 34: // Page Down
            case 32: // Spacebar
            case 40: // Down Key
              offset = 1;
              break;

            case 36: // Home
              offset = scope.currentSectionIndex;
              break;

            case 35: // End
              offset = (scope.totalSectons -  scope.currentSectionIndex) -1;
              break;

            default:
              break;
          }

          scope.$broadcast('transition', offset);
        });

        $document.on('mousewheel DOMMouseScroll MozMousePixelScroll', function(event){
          event.preventDefault();
          if(!scope.inTransition && !scope.quietPeriod){
            if(event.originalEvent.wheelDelta > 0){
              scope.$emit('transition', -1);
            }
            else{
              scope.$emit('transition', 1);
            }
          }
        });
      }
    }
  }]);
})();
