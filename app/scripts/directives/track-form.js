(function(){
  'use strict';

  angular.module('isseExpress')
    .directive('trackingCodeForm', function(){
      return {
        restrict: 'E',
        scope: {
          afterSubmit: '='
        },
        templateUrl: '/templates/tracking-code-form.html',
        controller: 'TrackingCodeFormController'
      }
  });
})();
