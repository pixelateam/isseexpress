(function(){
  'use strict';

  angular.module('isseExpress')
  .directive('showCase', function(){
    return {
      restrict: 'E',
      scope: {
        selectedItemChanged: '=?'
      },
      transclude: {
        title: '?showCaseTitle',
        content: 'showCaseContent'
      },
      controller: 'ShowCaseController',
      templateUrl: '/templates/show-case.html'
    }
  });
})();
