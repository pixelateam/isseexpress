(function() {
  'use strict';

  angular.module('isseExpress',
    [
      'ngAnimate',
      'ngTouch',
      'ngAria',
      'ngRoute',
      'ngMap',
      'angular-gestures'
    ]);

})();
